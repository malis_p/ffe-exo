import sys
import datetime
import calendar

#HANDLER FOR DATE AND DATE CALCULATION
class DateT():

	#Datetime.Date
	_Date = None

	def __init__(self, Year, Month, Day):
		if  calendar.monthrange(Year, Month)[1] < Day:
			self._Date = datetime.date(Year, Month, calendar.monthrange(Year, Month)[1])
		else:
			self._Date = datetime.date(Year, Month, Day)

	def GetExactDate(self):
		return self._Date

	def GetLastDayOfMonth(self):
		return datetime.date(self._Date.year, self._Date.month, calendar.monthrange(self._Date.year,self._Date.month)[1])

	def AddYearToDate(Date, YearsToAdd):
		if  calendar.monthrange(Date.year + YearsToAdd, Date.month)[1] < Date.day:
			return datetime.date(Date.year + YearsToAdd, Date.month, calendar.monthrange(Date.year + YearsToAdd, Date.month)[1])
		else:
			return datetime.date(Date.year + YearsToAdd, Date.month, Date.day)

#LINK BETWEEN BREVET AND LEGISLATION, MOST LOGIC IS HERE
class BrevetToLegislationLink():

	#Legislation
	_Legislation = ''

	#DATE de depot
	_SubmissionDate = None

	#DATE de délivré
	_DeliveryDate = None

	#INT
	_NumberOfYears = 20

	def __init__(self, Legislation, SubmissionDate, DeliveryDate):
		self._Legislation = Legislation
		self._SubmissionDate = SubmissionDate
		self._DeliveryDate = DeliveryDate

	def SetNumberOfYears(self, Years):
		self._NumberOfYears = Years

	def GetLegislation(self):
		return self._Legislation

	def GetSubmissionDate(self):
		return self._SubmissionDate

	def GetDeliveryDate(self):
		return self._DeliveryDate

	#RETURN THE FIRST PAYMENT DATET
	def GetFirstPaymentDate(self):
		if self.GetLegislation().IsPaymentOnDelivery() == True:
			if self.GetLegislation().IsPaymentEndOfMonth() == True:
				return self._DeliveryDate.GetLastDayOfMonth()
			else:
				return self._DeliveryDate.GetExactDate()
		else:
			if self.GetLegislation().IsPaymentEndOfMonth() == True:
				return self._SubmissionDate.GetLastDayOfMonth()
			else:
				return self._SubmissionDate.GetExactDate()

	#RETURN THE BASE DATET
	def GetBaseDate(self):
		if self.GetLegislation().IsBaseDateOnDelivery() == True:
			if self.GetLegislation().IsPaymentEndOfMonth() == True:
				return self._DeliveryDate.GetLastDayOfMonth()
			else:
				return self._DeliveryDate.GetExactDate()
		else:
			if self.GetLegislation().IsPaymentEndOfMonth() == True:
				return self._SubmissionDate.GetLastDayOfMonth()
			else:
				return self._SubmissionDate.GetExactDate()

	#RETURN THE EXPIRACY DATET
	def GetExpiracyDate(self):
		if self.GetLegislation().IsBaseDateOnDelivery() == True:
			if self.GetLegislation().IsPaymentEndOfMonth() == True:
				return DateT.AddYearToDate(self._DeliveryDate.GetLastDayOfMonth(), self._NumberOfYears)
			else:
				return DateT.AddYearToDate(self._DeliveryDate.GetExactDate(), self._NumberOfYears)
		else:
			if self.GetLegislation().IsPaymentEndOfMonth() == True:
				return DateT.AddYearToDate(self._SubmissionDate.GetLastDayOfMonth(), self._NumberOfYears)
			else:
				return DateT.AddYearToDate(self._SubmissionDate.GetExactDate(), self._NumberOfYears)

	#PRINT PAYMENT DATET  ON CONSOLE
	def ListPayments(self):
		
		start_month = self.GetBaseDate().month
		start_year = self.GetBaseDate().year

		last_month = self.GetExpiracyDate().month
		last_year = self.GetExpiracyDate().year

		current_day = self.GetBaseDate().day
		current_year = start_year
		while current_year < last_year + 1:
			if current_year == start_year:
				current_month = start_month
			else:
				current_month = 1

			while (current_month < 13 and current_year < last_year) or (current_year == last_year and current_month < last_month):
				Debug('Payment On [' + str(DateT(current_year, current_month, current_day).GetExactDate()) + ']')
				current_month = current_month + 1
			current_year = current_year + 1

	#PRINT EXPIRACY DATE ON CONSOLE
	def ExpiracyDate(self):
		Debug('Expiracy on [' + str(self.GetExpiracyDate()) + ']')

#DATA HANDLER FOR BREVET
class Brevet():
	
	#String
	_Data = ''

	#List of BrevetToLegislationLink
	_Links = []

	def __init__(self, Data): 
		self._Data = Data
		#Debug('Created [' + self._Data + '] Patent')

	def SetNumberOfYears(self, Years):
		for item in self._Links:
			item.SetNumberOfYears(Years)

	def ApplyToLegislation(self, Legislation, SubmissionDate, DeliveryDate):
		Link = BrevetToLegislationLink(Legislation, SubmissionDate, DeliveryDate)
		self._Links.append(Link)
		#Debug('Added [' + Legislation.GetId() + '] Legislation To [' + self._Data + '] Patent')

	def ListPayments(self):
		for item in self._Links:

			Debug('\n\n\nListing Payment for [' + self._Data + '][' + item.GetLegislation().GetId() + ']')
			Debug('Submission Date = [' + str(item.GetSubmissionDate().GetExactDate()) + ']')
			Debug('Delivery Date = [' + str(item.GetDeliveryDate().GetExactDate()) +']')
			Debug('Base Date = [' + str(item.GetBaseDate()) + ']')
			Debug('First Payment = [' + str(item.GetFirstPaymentDate()) + ']')
			Debug('Expiracy Date =' + str(item.GetExpiracyDate()) + ']')
			Debug('--------------')
			item.ListPayments()

	def ExpiracyDate(self):
		for item in self._Links:
			Debug('\n\nExpiracy Date for [' + self._Data + '][' + item.GetLegislation().GetId() + '][' + str(item.GetSubmissionDate().GetExactDate()) + '][' + str(item.GetDeliveryDate().GetExactDate()) +']')
			item.ExpiracyDate()

#DATA HANDLER FOR LEGISLATION
class Legislation():

	#String (Id)
	_Country = ''

	#BOOL False=Delivery True=Submission
	_PaymentOnDelivery = False

	#BOOL False=Delivery True=Submission
	_BaseDateOnDelivery = False

	#BOOL False=ExactDate, True=EndOfMonth
	_PaymentEndOfMonth = False

	def __init__(self, CountryName, PaymentOnDelivery, BaseDateOnDelivery, PaymentEndOfMonth): 
		self._Country = CountryName
		self._PaymentOnDelivery = PaymentOnDelivery
		self._BaseDateOnDelivery = BaseDateOnDelivery
		self._PaymentEndOfMonth = PaymentEndOfMonth

	def GetId(self):
		return self._Country

	def IsPaymentOnDelivery(self):
		return self._PaymentOnDelivery

	def IsBaseDateOnDelivery(self):
		return self._BaseDateOnDelivery

	def IsPaymentEndOfMonth(self):
		return self._PaymentEndOfMonth

#PRINTING TO TERMINAL
def Debug(message):
	sys.stdout.write(message + '\n')



#EXAMPLE EXECUTION HERE
def main():
	Debug('START\n\n')
	Legal = {}

	Legal['FR'] = Legislation('FR', True, True, False)
	Legal['US'] = Legislation('US', False, False, True)
	Legal['UK'] = Legislation('UK', True, True, True)
	

	TechnicalPatent = Brevet('Technical Patent')
	TechnicalPatent.ApplyToLegislation(Legal['FR'], DateT(2000, 1, 13), DateT(2000, 2, 1))
	TechnicalPatent.ApplyToLegislation(Legal['US'], DateT(2000, 1, 13), DateT(2000, 2, 1))
	TechnicalPatent.ApplyToLegislation(Legal['UK'], DateT(2000, 1, 13), DateT(2000, 2, 1))
	TechnicalPatent.SetNumberOfYears(5)

	TechnicalPatent.ListPayments()
	TechnicalPatent.ExpiracyDate()

	Debug('\n\nEND\n\n')





main()