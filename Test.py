import unittest
from Exo import *


class TestStringMethods(unittest.TestCase):

	def test_scenario(self):
		Legal = {}

		Legal['FR'] = Legislation('FR', True, True, False)
		Legal['US'] = Legislation('US', False, False, True)
		Legal['UK'] = Legislation('UK', True, True, True)
		

		TechnicalPatent = Brevet('Technical Patent')
		TechnicalPatent.ApplyToLegislation(Legal['FR'], DateT(2000, 1, 13), DateT(2000, 2, 1))
		TechnicalPatent.ApplyToLegislation(Legal['US'], DateT(2000, 1, 13), DateT(2000, 2, 1))
		TechnicalPatent.ApplyToLegislation(Legal['UK'], DateT(2000, 1, 13), DateT(2000, 2, 1))
		TechnicalPatent.SetNumberOfYears(5)

		TechnicalPatent.ListPayments()
		TechnicalPatent.ExpiracyDate()

		self.assertTrue(Legal['FR'].IsPaymentOnDelivery())
		self.assertTrue(Legal['FR'].IsBaseDateOnDelivery())
		self.assertFalse(Legal['FR'].IsPaymentEndOfMonth())

		self.assertFalse(Legal['US'].IsPaymentOnDelivery())
		self.assertFalse(Legal['US'].IsBaseDateOnDelivery())
		self.assertTrue(Legal['US'].IsPaymentEndOfMonth())

		self.assertTrue(Legal['UK'].IsPaymentOnDelivery())
		self.assertTrue(Legal['UK'].IsBaseDateOnDelivery())
		self.assertTrue(Legal['UK'].IsPaymentEndOfMonth())

if __name__ == '__main__':
    unittest.main()